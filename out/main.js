var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cli_1 = require("./modules/cli");
const parser_1 = require("./modules/parser");
const io_1 = require("./modules/io");
const log_1 = require("./modules/log");
class App {
    static main() {
        cli_1.CLI.isInputValid()
            .then((args) => __awaiter(this, void 0, void 0, function* () {
            if (args == "HALT")
                return; // no further operation necessary
            // run operations to produce output from template
            let outTemplate = yield parser_1.TemplateParser.processTemplate(args.templateName);
            if (outTemplate == "HALT")
                return; // used by template/all
            io_1.FileWriter.ymlWith(outTemplate, args.outPath);
        }))
            .catch((e) => {
            new log_1.Log(e.message).error().print();
        });
    }
}
exports.default = App;
