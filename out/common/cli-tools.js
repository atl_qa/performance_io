var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Spinner = void 0;
const process_1 = __importDefault(require("process"));
class Spinner {
    static start() {
        Spinner.isOn = true;
        process_1.default.stdout.write("\x1B[?25l");
        const spinners = ["-", "\\", "|", "/"];
        let index = 0;
        Spinner.spinnerInterval = setInterval(() => {
            let line = spinners[index];
            process_1.default.stdout.write(line);
            process_1.default.stdout.moveCursor(-1, 0);
            index = (index == spinners.length - 1) ? 0 : (index + 1);
        }, 100);
    }
    static stop() {
        clearInterval(Spinner.spinnerInterval);
        process_1.default.stderr.write('\x1B[?25h');
        Spinner.isOn = false;
    }
}
exports.Spinner = Spinner;
Spinner.spinnerInterval = null;
Spinner.isOn = false;
