var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Http = void 0;
const cli_tools_1 = require("./cli-tools");
var request = require('request');
class Http {
    static post(url, body) {
        return __awaiter(this, void 0, void 0, function* () {
            let promise = new Promise((resolve, reject) => {
                cli_tools_1.Spinner.start();
                request.post(url, { json: body }, function (error, response, body) {
                    cli_tools_1.Spinner.stop();
                    resolve({ error: response.statusCode != 200, body, response });
                });
            });
            return promise;
        });
    }
    static get(url) {
        return __awaiter(this, void 0, void 0, function* () {
            let promise = new Promise((resolve, reject) => {
                cli_tools_1.Spinner.start();
                request(url, (error, response, body) => {
                    cli_tools_1.Spinner.stop();
                    resolve({ error: response.statusCode != 200, body, response });
                });
            });
            return promise;
        });
    }
    static delete(url) {
        return __awaiter(this, void 0, void 0, function* () {
            let promise = new Promise((resolve, reject) => {
                cli_tools_1.Spinner.start();
                request.delete(url, function (error, response, body) {
                    cli_tools_1.Spinner.stop();
                    resolve({ error: response.statusCode != 204, body, response });
                });
            });
            return promise;
        });
    }
}
exports.Http = Http;
