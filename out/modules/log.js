Object.defineProperty(exports, "__esModule", { value: true });
exports.Log = void 0;
class Log {
    constructor(text) {
        this.text = text;
    }
    print() {
        console.log("patest: " + this.text);
    }
    get() {
        return this.text;
    }
    error() {
        this.text = '\x1b[31m' + this.text + '\x1b[0m';
        return this;
    }
    warn() {
        this.text = '\x1b[33m' + this.text + '\x1b[0m';
        return this;
    }
    green() {
        this.text = '\x1b[32m' + this.text + '\x1b[0m';
        return this;
    }
}
exports.Log = Log;
