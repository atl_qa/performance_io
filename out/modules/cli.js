var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CLI = void 0;
const entrypoint_1 = require("../entrypoint");
const log_1 = require("./log");
const io_1 = require("./io");
const fs_1 = __importDefault(require("fs"));
const http_1 = require("../common/http");
class CLI {
    static isInputValid() {
        let options = ["help", "version", "list", "ls", "get", "upload", "delete"];
        if (!options.includes(process.argv[2]))
            return Promise.reject(new Error("invalid command '" + process.argv[2] + "'"));
        return CLI[process.argv[2]]();
    }
    /* --- COMMANDS --- */
    static get() {
        let errorMsg = "args not recognized";
        if (process.argv[5])
            return Promise.reject(new Error(errorMsg));
        let args = {
            templateName: process.argv[3] || "",
            outPath: process.argv[4] || undefined
        };
        if (args.templateName == "")
            return Promise.reject(new Error(errorMsg));
        return Promise.resolve(args);
    }
    static upload() {
        return __awaiter(this, void 0, void 0, function* () {
            let ans = yield new io_1.InputSequentialCollector().getUserValues(["path", "template name", "author"]);
            for (let a of ans)
                if (!a || a == "")
                    return Promise.reject("args not recognized");
            let templateContent = fs_1.default.readFileSync(ans[0], "utf-8");
            let res = yield http_1.Http.post(entrypoint_1.templateServerURL, { name: ans[1], value: templateContent, author: ans[2] });
            if (res.error)
                return Promise.reject("something went wrong");
            let template = res.body.template;
            new log_1.Log(`upload successful - name:'${template.name}' | author: '${template.author}' | timestamp: '${template.createdAt}'`).green().print();
            return Promise.resolve("HALT");
        });
    }
    static delete() {
        return __awaiter(this, void 0, void 0, function* () {
            if (process.argv.length != 4)
                return Promise.reject("args not recognized");
            let templateName = process.argv[3];
            let res = yield http_1.Http.delete(entrypoint_1.templateServerURL + "/" + templateName);
            if (res.error)
                return Promise.reject("something went wrong");
            new log_1.Log(`removed '${templateName}' successfully`).green().print();
            return Promise.resolve("HALT");
        });
    }
    static ls() { return CLI.list(); }
    static list() {
        if (process.argv[4])
            return Promise.reject(new Error("args not recognized"));
        let args = {
            templateName: "all"
        };
        return Promise.resolve(args);
    }
    /**
     * print help page
     */
    static help() {
        console.log(entrypoint_1.MANPage);
        return Promise.resolve("HALT");
    }
    /**
     * print version of app
     */
    static version() {
        new log_1.Log("v" + entrypoint_1.version).green().print();
        return Promise.resolve("HALT");
    }
}
exports.CLI = CLI;
