var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileWriter = exports.InputSequentialCollector = void 0;
const fs_1 = __importDefault(require("fs"));
const readline = require('readline');
/**
 * Input consumer
 */
class InputSequentialCollector {
    constructor() {
        this.rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout,
        });
    }
    getUserValues(vars) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!vars || vars.length == 0) {
                this.rl.close();
                return Promise.resolve([]);
            }
            let ans = yield this.__aux_getUserValues(vars);
            this.rl.close();
            return ans;
        });
    }
    __aux_getUserValues(vars) {
        return __awaiter(this, void 0, void 0, function* () {
            let ans = [];
            if (vars.length == 0)
                return Promise.resolve([]);
            ans.push(yield new Promise(resolve => this.rl.question(vars[0] + ": ", resolve)));
            ans.push(...yield this.getUserValues(vars.slice(1)));
            return Promise.resolve(ans);
        });
    }
}
exports.InputSequentialCollector = InputSequentialCollector;
/**
 * output writer
 */
class FileWriter {
    static ymlWith(outTemplate, outPath) {
        if (!outPath || outPath == "./")
            outPath = "./patest-out.yml";
        fs_1.default.writeFileSync(outPath, outTemplate);
    }
}
exports.FileWriter = FileWriter;
