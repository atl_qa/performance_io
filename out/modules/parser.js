var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TemplateParser = void 0;
const http_1 = require("../common/http");
const entrypoint_1 = require("../entrypoint");
const io_1 = require("./io");
class TemplateParser {
    static processTemplate(name) {
        return __awaiter(this, void 0, void 0, function* () {
            // pull template value from server
            let res = yield http_1.Http.get(entrypoint_1.templateServerURL + "/" + name);
            if (res.error) {
                throw new Error("template not found");
            }
            else {
                if (name == "all") { // do not retrun vars; print list only
                    TemplateParser.listTemplates(res);
                    return Promise.resolve("HALT");
                }
                // get vars and user input for specific template
                let template = JSON.parse(res.body)["template"];
                let vars = TemplateParser.pullVarsFromString(template.value);
                let defaults = TemplateParser.pullVarsDefaultsFromString(template.value);
                let userValues = yield new io_1.InputSequentialCollector().getUserValues(vars);
                for (let i in userValues)
                    if (!userValues[i] || userValues[i] == "")
                        userValues[i] = defaults[i];
                // replace vars with user input for specific template
                var matches = template.value.match(/({{)(.*)(}})/g);
                let outTemplate = template.value;
                for (let i in matches)
                    outTemplate = outTemplate.split(matches[i]).join(userValues[i]);
                return Promise.resolve(outTemplate);
            }
        });
    }
    /**
     * get vars from str using format '{{var:default}}'
     */
    static pullVarsFromString(str) {
        let vars = str.match(/({{)(.*)(}})/g).map(v => v.split(":")[0].slice(2));
        return vars;
    }
    /**
     * get defaults from str using format '{{var:default}}'
     */
    static pullVarsDefaultsFromString(str) {
        let defaults = str.match(/({{)(.*)(}})/g).map(v => {
            let def = v.split(":")[1];
            return def.slice(0, def.length - 2);
        });
        return defaults;
    }
    /**
     * list all templates from server
     */
    static listTemplates(res) {
        let templates = JSON.parse(res.body)["templates"];
        console.log(" ");
        for (let i in templates) {
            console.log(`  ${1 + Number(i)}. name: '${templates[i].name}'\t| author: '${templates[i].author}'\t| timestamp: '${new Date(templates[i].createdAt).toLocaleString()}'`);
        }
        console.log("");
    }
}
exports.TemplateParser = TemplateParser;
