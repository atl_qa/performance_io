#!/usr/bin/env node
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MANPage = exports.templateServerURL = exports.version = void 0;
const main_1 = __importDefault(require("./main"));
exports.version = "0.0.1";
exports.templateServerURL = "https://patest-template.herokuapp.com/template";
exports.MANPage = `
    -------------------------------------------------------------
    Command                           | Description
    -------------------------------------------------------------
    list                              | show all templates 
    get     <templateName> <outPath?> | fetch and run template
    upload  <templatePath>            | add new template from path
    delete  <templateName>            | remove template from server

`;
// start main process
main_1.default.main();
