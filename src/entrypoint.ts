#!/usr/bin/env node

import App from "./main"

export const version = "0.0.1"
export const templateServerURL = "https://patest-template.herokuapp.com/template"


export const MANPage = 
`
    -------------------------------------------------------------
    Command                           | Description
    -------------------------------------------------------------
    list                              | show all templates 
    get     <templateName> <outPath?> | fetch and run template
    upload  <templatePath>            | add new template from path
    delete  <templateName>            | remove template from server

`

// start main process
App.main()
