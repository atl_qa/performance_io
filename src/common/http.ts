import { rejects } from "assert";
import { Spinner } from "./cli-tools";

var request = require('request');


export class Http {

    static async post(url:string, body:any):Promise<any>{
        let promise = new Promise((resolve, reject)=>{
            Spinner.start()
            request.post( url, { json: body },
                function (error, response, body) {
                    Spinner.stop()
                    resolve({error:response.statusCode!=200, body, response})
                }
            );
        })
        return promise
    }

    static async get(url:string):Promise<any>{
        let promise = new Promise((resolve, reject)=>{
            Spinner.start()
            request(url,(error, response, body)=>{
                Spinner.stop()
                resolve({error:response.statusCode!=200, body, response})
            })
        })
        return promise
    }

    static async delete(url:string):Promise<any>{
        let promise = new Promise((resolve, reject)=>{
            Spinner.start()
            request.delete( url,
                function (error, response, body) {
                    Spinner.stop()
                    resolve({error:response.statusCode!=204, body, response})
                }
            );
        })
        return promise
    }


}


