
import process from "process"

export class Spinner {

    static spinnerInterval:any = null
    static isOn = false

    static start(){
        Spinner.isOn = true
        process.stdout.write("\x1B[?25l")
        const spinners = ["-", "\\", "|", "/"]
        let index = 0

        Spinner.spinnerInterval = setInterval(()=>{
            let line = spinners[index]
            process.stdout.write(line)
            process.stdout.moveCursor(-1, 0);
            index = (index == spinners.length-1)?0:(index+1)
        }, 100)
    }

    static stop(){
        clearInterval(Spinner.spinnerInterval);
        process.stderr.write('\x1B[?25h')
        Spinner.isOn = false
    }
}