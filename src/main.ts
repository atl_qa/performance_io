import {CLI, CLIArgs} from "./modules/cli"
import {TemplateParser} from "./modules/parser"
import {InputSequentialCollector, FileWriter} from "./modules/io"
import {Log} from "./modules/log"

export default class App{


    static main(){
        
        CLI.isInputValid()
        .then(async (args:CLIArgs|string)=>{
            if(args == "HALT") return // no further operation necessary
            // run operations to produce output from template
            let outTemplate:string = await TemplateParser.processTemplate((args as CLIArgs).templateName)
            if(outTemplate == "HALT") return // used by template/all
            FileWriter.ymlWith(outTemplate, (args as CLIArgs).outPath)
        })
        .catch((e:Error)=>{
            new Log(e.message).error().print()
        })
    }


}
