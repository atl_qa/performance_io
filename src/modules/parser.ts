
import {Http} from "../common/http"
import {templateServerURL} from "../entrypoint"
import { InputSequentialCollector } from "./io"

export class TemplateParser{


    static async processTemplate(name:string):Promise<string>{
        // pull template value from server
        
        let res = await Http.get(templateServerURL+"/"+name)
        if(res.error){
            throw new Error("template not found")
        }else{
            if(name == "all"){ // do not retrun vars; print list only
                TemplateParser.listTemplates(res)
                return Promise.resolve("HALT")
            }
            // get vars and user input for specific template
            let template = JSON.parse(res.body)["template"]
            let vars: string[] = TemplateParser.pullVarsFromString(template.value)
            let defaults: string[] = TemplateParser.pullVarsDefaultsFromString(template.value)
            let userValues = await new InputSequentialCollector().getUserValues(vars)
            for(let i in userValues) if(!userValues[i] || userValues[i]=="") userValues[i]=defaults[i]
            
            // replace vars with user input for specific template
            var matches = template.value.match(/({{)(.*)(}})/g)
            let outTemplate = template.value
            for(let i in matches) outTemplate = outTemplate.split(matches[i]).join(userValues[i])
            return Promise.resolve(outTemplate)
        }
    }

    

    /**
     * get vars from str using format '{{var:default}}' 
     */
    static pullVarsFromString(str:string):string[]{
        let vars: string[] = str.match(/({{)(.*)(}})/g).map(v=>v.split(":")[0].slice(2))
        return vars
    }

    /**
     * get defaults from str using format '{{var:default}}' 
     */
    static pullVarsDefaultsFromString(str:string):string[]{
        let defaults: string[] = str.match(/({{)(.*)(}})/g).map(v=>{
            let def = v.split(":")[1]
            return def.slice(0,def.length-2)
        })
        return defaults
    }


    /**
     * list all templates from server
     */
    static listTemplates(res:any){
        let templates = JSON.parse(res.body)["templates"]
        console.log(" ")
        for(let i in templates){
            console.log(`  ${1+Number(i)}. name: '${templates[i].name}'\t| author: '${templates[i].author}'\t| timestamp: '${new Date(templates[i].createdAt).toLocaleString()}'`)
        }
        console.log("")
    }

}
