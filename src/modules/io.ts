import fs from "fs"

const readline = require('readline');




/**
 * Input consumer
 */
export class InputSequentialCollector {

    rl:any

    constructor(){
        this.rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout,
        });
    }

    async getUserValues(vars: string[]):Promise<string[]>{
        if(!vars || vars.length == 0 ) {
            this.rl.close()
            return Promise.resolve([])
        }
        let ans = await this.__aux_getUserValues(vars)
        this.rl.close()
        return ans
    }

    async __aux_getUserValues(vars: string[]):Promise<string[]>{
        let ans: string[] = []
        if(vars.length == 0) return Promise.resolve([])
        ans.push(await new Promise(resolve => this.rl.question(vars[0]+": ", resolve)))
        ans.push(...await this.getUserValues(vars.slice(1)))
        return Promise.resolve(ans)
    }

}



/**
 * output writer
 */
export class FileWriter{


    static ymlWith(outTemplate:string, outPath?:string){
        if(!outPath || outPath == "./") outPath = "./patest-out.yml"
        fs.writeFileSync(outPath,outTemplate)
    }

}
