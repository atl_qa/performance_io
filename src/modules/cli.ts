import {version, MANPage, templateServerURL} from "../entrypoint"
import { Log } from "./log"
import { InputSequentialCollector } from "./io"
import fs from "fs"
import { Http } from "../common/http"


export class CLI{
    
    static isInputValid():Promise<any>{
        let options:string[] = ["help", "version", "list","ls", "get", "upload", "delete"]
        if(!options.includes(process.argv[2])) return Promise.reject(new Error("invalid command '"+process.argv[2]+"'"))
        return CLI[process.argv[2]]()
    }

    /* --- COMMANDS --- */

    static get():Promise<any>{
        let errorMsg = "args not recognized"
        if(process.argv[5]) return Promise.reject(new Error(errorMsg))
        let args:CLIArgs = {
            templateName: process.argv[3] || "",
            outPath: process.argv[4] || undefined
        }
        if(args.templateName == "") return Promise.reject(new Error(errorMsg))
        return Promise.resolve(args)
    }

    static async upload():Promise<any>{
        let ans = await new InputSequentialCollector().getUserValues(["path", "template name", "author"])
        for(let a of ans) if(!a || a == "") return Promise.reject("args not recognized")
        let templateContent = fs.readFileSync(ans[0], "utf-8")
        let res = await Http.post(templateServerURL,{name:ans[1], value:templateContent, author:ans[2]})
        if(res.error) return Promise.reject("something went wrong")
        let template = res.body.template
        new Log(`upload successful - name:'${template.name}' | author: '${template.author}' | timestamp: '${template.createdAt}'`).green().print()
        return Promise.resolve("HALT")
    }

    static async delete():Promise<any>{
        if(process.argv.length != 4) return Promise.reject("args not recognized")
        let templateName = process.argv[3]
        let res = await Http.delete(templateServerURL+"/"+templateName)
        if(res.error) return Promise.reject("something went wrong")
        new Log(`removed '${templateName}' successfully`).green().print()
        return Promise.resolve("HALT")
    }

    static ls():Promise<any>{return CLI.list()}
    static list():Promise<any>{
        if(process.argv[4]) return Promise.reject(new Error("args not recognized"))
        let args:CLIArgs = {
            templateName: "all"
        }
        return Promise.resolve(args)
    }


    /**
     * print help page
     */
    static help():Promise<any>{
        console.log(MANPage)
        return Promise.resolve("HALT")
    }

    /**
     * print version of app
     */
    static version():Promise<any>{
        new Log("v"+version).green().print()
        return Promise.resolve("HALT")
    }

}


export interface CLIArgs{
    templateName: string,
    outPath?: string
}