

export class Log{

    constructor(private text:string){

    }

    print():void{
        console.log("patest: "+this.text)
    }

    get():string{
        return this.text
    }

    error():Log{
        this.text = '\x1b[31m'+this.text+'\x1b[0m'
        return this
    }

    warn():Log{
        this.text = '\x1b[33m'+this.text+'\x1b[0m'
        return this
    }

    green():Log{
        this.text = '\x1b[32m'+this.text+'\x1b[0m'
        return this
    }




}